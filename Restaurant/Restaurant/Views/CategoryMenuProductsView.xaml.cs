﻿using Restaurant.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Restaurant.Views
{
    /// <summary>
    /// Interaction logic for CategoryMenuProductsView.xaml
    /// </summary>
    public partial class CategoryMenuProductsView : UserControl
    {
        public CategoryMenuProductsView()
        {
            InitializeComponent();
        }

        private void menuProduct_Click(object sender, RoutedEventArgs e)
        {
            Window mainWindow = Window.GetWindow(this);
            ProductView productView = new ProductView();
            productView.DataContext = this.DataContext;
            mainWindow.DataContext = productView;

        }

        private void menuProduct_Click(object sender, MouseButtonEventArgs e)
        {
            Window mainWindow = Window.GetWindow(this);
            mainWindow.DataContext = new MenuProductVM(((CategoryMenuProductsVM)DataContext).SelectedMenuProduct);
        }
    }


}
