﻿using Restaurant.Models.EntityLayer;
using Restaurant.ViewModels;
using Restaurant.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Restaurant
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //ContentControler.Content = new MenuCategoryView();
            Application.Current.Resources["LoggedUser"] = null;
            DataContext = new MenuCategoryVM();    
        }

        private void LogoBtn_Click(object sender, RoutedEventArgs e)
        {
            DataContext = new MenuCategoryVM();
        }

        private void LogInBtn_Click(object sender, RoutedEventArgs e)
        {
            User loggedUser = (User)Application.Current.Resources["LoggedUser"];
            if (loggedUser != null)
            {
                MessageBox.Show("Trebuie sa va delogati!");
            }
            else
            {
                DataContext = new LoginView();
            }
        }

        private void ResigterBtn_Click(object sender, RoutedEventArgs e)
        {
            User loggedUser = (User)Application.Current.Resources["LoggedUser"];
            if (loggedUser != null)
            {
                MessageBox.Show("Trebuie sa va delogati!");
            }
            else
            {
                DataContext = new RegisterView();
            }
        }

        private void SignOutBtn_Click(object sender, RoutedEventArgs e)
        {
            User loggedUser = (User)Application.Current.Resources["LoggedUser"];
            if (loggedUser != null)
            {
                Application.Current.Resources["LoggedUser"] = null;
            }
            else
            {
                MessageBox.Show("Nu sunteti logat!");
            }
        }

        private void CartBtn_Click(object sender, RoutedEventArgs e)
        {
            User loggedUser = (User)Application.Current.Resources["LoggedUser"];
            if (loggedUser != null)
            {
                DataContext = new CartVM(loggedUser);
            }
            else
            {
                MessageBox.Show("Nu sunteti logat!");
            }
        }

        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            DataContext = new SearchVM();
        }
    }
    }

