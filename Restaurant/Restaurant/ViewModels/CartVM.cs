﻿using Restaurant.Models.DataAccessLayer;
using Restaurant.Models.EntityLayer;
using Restaurant.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Restaurant.ViewModels
{
    class CartVM : BasePropertyChanged
    {
        //CartBLL cartBLL = new CartBLL();

        OrderDAL orderDAL = new OrderDAL();

        private double price;
        public double Price
        {
            get
            {

               return price;
            }
            set
            {
                price = value;
                NotifyPropertyChanged("Price");
            }
        }
        public ObservableCollection<CartMenuProduct> CartMenuProductsList { get; set; }

        private string message;

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }

        private CartMenuProduct selectedCartProduct;
        public CartMenuProduct SelectedCartProduct
        {
            get
            {
                return selectedCartProduct;
            }
            set
            {
                selectedCartProduct = value;
                NotifyPropertyChanged("SelectedCartProduct");
            }
        }

        private User loggedUser;
        public CartVM(User user)
        {
            loggedUser = user;
            CartMenuProductsList = new ObservableCollection<CartMenuProduct>(user.CartProductsList);
            foreach (CartMenuProduct product in loggedUser.CartProductsList)
            {
                Price += (double)(product.Quantity * product.MenuProduct.Price);
            }
        }

        #region Commands

        private ICommand removeFromCartCommand;
        public ICommand RemoveFromCartCommand
        {
            get
            {

                if (removeFromCartCommand == null)
                {
                    removeFromCartCommand = new RelayCommand<CartMenuProduct>(RemoveFromCart);
                }
                return removeFromCartCommand;

            }
        }

        private ICommand placeOrderCommand;
        public ICommand PlaceOrderCommand
        {
            get
            {
                if (placeOrderCommand == null)
                {
                    placeOrderCommand = new RelayCommand<CartMenuProduct>(AddOrder);
                }
                return placeOrderCommand;

            }
        }

        #endregion

        #region Functions

        internal void RemoveFromCart(object param)
        {
            if(selectedCartProduct == null)
            {
                return;
            }
            Price -= (double)(selectedCartProduct.Quantity * selectedCartProduct.MenuProduct.Price);
            loggedUser.CartProductsList.Remove(selectedCartProduct);
            CartMenuProductsList.Remove(selectedCartProduct);


        }

        internal void AddOrder(object param)
        {
            orderDAL.AddOrder(loggedUser);
            loggedUser.CartProductsList.Clear();
            CartMenuProductsList.Clear();
            Message = "Comanda adaugata";

        }

        #endregion
    }
}
