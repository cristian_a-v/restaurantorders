﻿using Restaurant.Models.BusinessLogicLayer;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.ViewModels
{
    class CategoryMenuProductsVM : BasePropertyChanged
    {
        MenuProductBLL menuProductyBLL = new MenuProductBLL();

        private MenuProduct selectedMenuProduct;

        public MenuProduct SelectedMenuProduct
        {
            get
            {
                return selectedMenuProduct;
            }
            set
            {
                selectedMenuProduct = value;
                NotifyPropertyChanged("SelectedMenuProduct");
            }
        }

        MenuCategory selectedMenuCategory;
        MenuCategory SelectedMenuCategory
        {
            get
            {
                return selectedMenuCategory;
            }
            set
            {
                selectedMenuCategory = value;
                NotifyPropertyChanged("SelectedMenuCategory");
            }
        }
        public CategoryMenuProductsVM(MenuCategory selectedMenuCategory)
        {
            MenuProductsList = menuProductyBLL.GetMenuProductForCategory(selectedMenuCategory);
            SelectedMenuCategory = selectedMenuCategory;
        }

        public CategoryMenuProductsVM()
        {

        }

        #region Data Members

        private string errorMessage;
        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }
            set
            {
                errorMessage = value;
                NotifyPropertyChanged("ErrorMessage");
            }
        }

        public ObservableCollection<MenuProduct> MenuProductsList
        {
            get
            {
                return menuProductyBLL.MenuProductList;
            }
            set
            {
                menuProductyBLL.MenuProductList = value;
            }
        }

        #endregion
    }
}
