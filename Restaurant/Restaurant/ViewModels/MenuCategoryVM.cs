﻿using Restaurant.Models.BusinessLogicLayer;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.ViewModels
{
    class MenuCategoryVM : BasePropertyChanged
    {
        MenuCategoryBLL menuCategoryBLL = new MenuCategoryBLL();
        public MenuCategory SelectedCategory { get; set; }
        public MenuCategoryVM()
        {
            MenuCategoryList = menuCategoryBLL.GetAllActiveMenuCategories();
        }

        #region Data Members

        private string errorMessage;
        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }
            set
            {
                errorMessage = value;
                NotifyPropertyChanged("ErrorMessage");
            }
        }

        public ObservableCollection<MenuCategory> MenuCategoryList
        {
            get
            {
                return menuCategoryBLL.MenuCategoriyList;
            }
            set
            {
                menuCategoryBLL.MenuCategoriyList = value;
            }
        }

        #endregion
    }
}

