﻿using Restaurant.Exceptions;
using Restaurant.Models.BusinessLogicLayer;
using Restaurant.Models.DataAccessLayer;
using Restaurant.Models.EntityLayer;
using Restaurant.ViewModels.Commands;
using Restaurant.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Restaurant.ViewModels
{
    class UserVM : BasePropertyChanged
    {
        UserDAL userDAL = new UserDAL();

        public UserVM()
        {

        }

        #region Data Members

        private string errorMessage;
        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }
            set
            {
                errorMessage = value;
                NotifyPropertyChanged("ErrorMessage");
            }
        }

        private string message;
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }

        private string firstName;
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
                NotifyPropertyChanged("FirstName");
            }
        }

        private string lastName;
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
                NotifyPropertyChanged("LastName");
            }
        }

        private string email;
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
                NotifyPropertyChanged("Email");
            }
        }

        private string phoneNumber;
        public string PhoneNumber
        {
            get
            {
                return phoneNumber;
            }
            set
            {
                phoneNumber = value;
                NotifyPropertyChanged("PhoneNumber");
            }
        }

        private string address;
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
                NotifyPropertyChanged("Address");
            }
        }

        private string password;
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
                NotifyPropertyChanged("Password");
            }
        }

        #endregion

        #region Command Members

        //asta este pt butonul Register
        private ICommand registerCommand;
        public ICommand RegisterCommand
        {
            get
            {

                if (registerCommand == null)
                {
                    registerCommand = new RelayCommand<User>(AddPerson);
                }
                return registerCommand;

            }
        }

        private ICommand loginUserCommand;
        public ICommand LoginUserCommand
        {
            get
            {

                if (loginUserCommand == null)
                {
                    loginUserCommand = new RelayCommand<User>(LogInUser);
                }
                return loginUserCommand;

            }
        }

        private ICommand loginAdminCommand;
        public ICommand LoginAdminCommand
        {
            get
            {

                if (loginAdminCommand == null)
                {
                    loginAdminCommand = new RelayCommand<User>(LogInAdmin);
                }
                return loginAdminCommand;

            }
        }

        #endregion

        #region Command Functions

        internal void AddPerson(object param)
        {
            try
            {
                if (String.IsNullOrEmpty(FirstName))
                {
                    throw new RestaurantException("FirstName trebuie sa fie precizat");
                }
                if (String.IsNullOrEmpty(LastName))
                {
                    throw new RestaurantException("LastName trebuie sa fie precizat");
                }
                if (String.IsNullOrEmpty(Email))
                {
                    throw new RestaurantException("Adresa de email trebuie sa fie precizata");
                }
                if (String.IsNullOrEmpty(PhoneNumber))
                {
                    throw new RestaurantException("Numarul de telefon sa fie precizat");
                }
                if (!PhoneNumber.All(Char.IsDigit))
                {
                    throw new RestaurantException("Numarul de telefon nu trebuie sa contina litere");
                }
                if (String.IsNullOrEmpty(Address))
                {
                    throw new RestaurantException("Adresa trebuie sa fie precizata");
                }
                if (String.IsNullOrEmpty(Password))
                {
                    throw new RestaurantException("Parola trebuie sa fie precizata");
                }
                if (userDAL.IsUserWithEmail(email))
                {
                    throw new RestaurantException("Exista deja un utilizator cu acest Email");
                }
            }
            catch (RestaurantException re)
            {
                ErrorMessage = re.Message;
                return;
            }
            userDAL.AddUser(new User
            {
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                PhoneNumber = phoneNumber,
                Address = address,
                Password = password
            });

            FirstName = String.Empty;
            LastName = String.Empty;
            Email = String.Empty;
            PhoneNumber = String.Empty;
            Address = String.Empty;
            Password = String.Empty;
            Message = "Registration Successful";
        }

        internal void LogInUser(object param)
        {
            if (String.IsNullOrEmpty(email))
            {
                ErrorMessage = "Insert valid Email";
                return;
            }
            if (String.IsNullOrEmpty(password))
            {
                ErrorMessage = "Insert valid Password";
                return;
            }
            User loggedUser = userDAL.GetUserEmailPassword(email, password);
            if(String.IsNullOrEmpty(loggedUser.FirstName))
            {
                ErrorMessage = "Incorrect Email or Password";
                return;
            }
            Application.Current.Resources["LoggedUser"] = loggedUser;
            Message = "Logat cu succes " + ((User)Application.Current.Resources["LoggedUser"]).FirstName;


        }

        internal void LogInAdmin(object param)
        {
            if(String.IsNullOrEmpty(email))
            {
                ErrorMessage = "Insert valid Email";
                return;
            }
            if (String.IsNullOrEmpty(password))
            {
                ErrorMessage = "Insert valid Password";
                return;
            }
            User loggedAdmin = userDAL.GetAdminEmailPassword(email, password);
            if (String.IsNullOrEmpty(loggedAdmin.FirstName))
            {
                ErrorMessage = "Incorrect Email or Password";
                return;
            }
            OrdersView orders = new OrdersView();
            orders.DataContext = new OrdersVM();
            orders.Show();
        }
        #endregion
    }
}

