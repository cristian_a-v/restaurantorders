﻿using Restaurant.Models.BusinessLogicLayer;
using Restaurant.Models.EntityLayer;
using Restaurant.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Restaurant.ViewModels
{
    class MenuProductVM : BasePropertyChanged
    {
        MenuProductBLL menuProductyBLL = new MenuProductBLL();

        private MenuProduct selectedMenuProduct;

        public MenuProduct SelectedMenuProduct
        {
            get
            {
                return selectedMenuProduct;
            }
            set
            {
                selectedMenuProduct = value;
                NotifyPropertyChanged("SelectedMenuProduct");
            }
        }

        public ObservableCollection<Product> ProductsList
        {
            get
            {
                return menuProductyBLL.ProductList;
            }
            set
            {
                menuProductyBLL.ProductList = value;
            }
        }

        private string message;

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged("Message");
            }
        }

        private int quantity = 1;
        public int Quantity
        {
            get
            {
                return quantity;
            }
            set
            {
                quantity = value;
                NotifyPropertyChanged("Quantity");
            }
        }
        public MenuProductVM(MenuProduct selectedMenuProduct)
        {
            ProductsList = menuProductyBLL.GetProductsForMenuProduct(selectedMenuProduct);
            SelectedMenuProduct = selectedMenuProduct;
        }

        private ICommand addToCartCommand;
        public ICommand AddToCartCommand
        {
            get
            {

                if (addToCartCommand == null)
                {
                    addToCartCommand = new RelayCommand<MenuProduct>(AddToCart);
                }
                return addToCartCommand;

            }
        }

        internal void AddToCart(object param)
        {
            User user = (User)Application.Current.Resources["LoggedUser"];
            if(user == null)
            {
                Message = "Trebuie sa va logati pentru a adauga in cos";
                return;
            }
            List<CartMenuProduct> currentCart = user.CartProductsList;
            
            if (currentCart.Select(x=> x.MenuProduct).Contains(selectedMenuProduct))
            {
                currentCart.Find(x => x.MenuProduct == selectedMenuProduct).Quantity += this.quantity;
                Message = "Produs deja in cos, cantitate modificata";
                return;
            }
            if(Quantity < 1)
            {
                Message = "Cantitate Invalida";
                return;

            }
            currentCart.Add(new CartMenuProduct
            {
                MenuProduct = selectedMenuProduct,
                Quantity = Quantity
            }) ;
            Message = "Produs adaugat cu succes!";


        }
    }
}
