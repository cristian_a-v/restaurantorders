﻿using Restaurant.Models.BusinessLogicLayer;
using Restaurant.Models.EntityLayer;
using Restaurant.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace Restaurant.ViewModels
{
    class SearchVM : BasePropertyChanged
    {
        MenuProductBLL menuProductyBLL = new MenuProductBLL();

        private string keyWord;
        public string KeyWord
        {
            get
            {
                return keyWord;
            }
            set
            {
                keyWord = value;
                NotifyPropertyChanged("KeyWord");
            }
        }

        private MenuProduct selectedMenuProduct;

        public MenuProduct SelectedMenuProduct
        {
            get
            {
                return selectedMenuProduct;
            }
            set
            {
                selectedMenuProduct = value;
                NotifyPropertyChanged("SelectedMenuProduct");
            }
        }

        private ObservableCollection<MenuProduct> menuProductsList;
        public ObservableCollection<MenuProduct> MenuProductsList
        {
            get
            {
                return menuProductsList;
            }
            set
            {
                menuProductsList = value;
                NotifyPropertyChanged("MenuProductsList");
            }
        }

        #region Commands

        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get
            {

                if (searchCommand == null)
                {
                    searchCommand = new RelayCommand<MenuProduct>(Search);
                }
                return searchCommand;

            }
        }

        #endregion

        #region Functions
        internal void Search(object param)
        {
            MenuProductsList = menuProductyBLL.GetProductsByName(keyWord);
        }
        #endregion
    }
}
