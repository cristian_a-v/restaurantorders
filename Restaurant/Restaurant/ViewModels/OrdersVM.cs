﻿using Restaurant.Models.DataAccessLayer;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.ViewModels
{
    class OrdersVM : BasePropertyChanged
    {
        OrderDAL orderDAL = new OrderDAL();

        private Order selectedOrder;

        public Order SelectedOrder
        {
            get
            {
                return selectedOrder;
            }
            set
            {
                selectedOrder = value;
                NotifyPropertyChanged("SelectedOrder");
            }
        }
        public ObservableCollection<Order> OrdersList { get; set; }

        public OrdersVM()
        {
            OrdersList = orderDAL.GetAllOrders();
        }

    }
}
