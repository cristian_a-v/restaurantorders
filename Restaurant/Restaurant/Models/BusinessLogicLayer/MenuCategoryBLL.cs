﻿using Restaurant.Models.DataAccessLayer;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Restaurant.Models.BusinessLogicLayer
{
    class MenuCategoryBLL
    {
        public ObservableCollection<MenuCategory> MenuCategoriyList { get; set; }

        

        MenuCategoryDAL menuCategoryDAL = new MenuCategoryDAL();

        internal ObservableCollection<MenuCategory> GetAllActiveMenuCategories()
        {
            return menuCategoryDAL.GetAllActiveCategories();
        }


    }
}
