﻿using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.DataAccessLayer
{
    class OrderDAL
    {
        internal void AddOrder(User user)
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("spAddOrder", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramUserId = new SqlParameter("@UserId", (int)user.UserID);
                cmd.Parameters.Add(paramUserId);
                con.Open();
                int reader = Convert.ToInt32(cmd.ExecuteScalar());
                int orderId = Convert.ToInt32(reader);


                foreach (CartMenuProduct product in user.CartProductsList)
                {
                cmd = new SqlCommand("spAddOrderMenuProduct", con);
                cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter paramOrderId = new SqlParameter("@OrderId", orderId);
                    SqlParameter paramMenuProductId = new SqlParameter("@MenuProductId", product.MenuProduct.MenuProductId);
                    SqlParameter paramQuantity = new SqlParameter("@Quantity", product.Quantity);
                    cmd.Parameters.Add(paramOrderId);
                    cmd.Parameters.Add(paramMenuProductId);
                    cmd.Parameters.Add(paramQuantity);
                    cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                con.Close();
            }
        }

        internal ObservableCollection<Order> GetAllOrders()
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("spGetAllOrders", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                ObservableCollection<Order> result = new ObservableCollection<Order>();
                Random random = new Random();
                while (reader.Read())
                {
                    Order order = new Order()
                    {
                        OrderId = (int)reader[0],
                        Status = (string)reader[1],
                        UserId = (int)reader[2],
                        OrderNumber = random.Next(0,10000)

                };
                    result.Add(order);
                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }
    }
}
