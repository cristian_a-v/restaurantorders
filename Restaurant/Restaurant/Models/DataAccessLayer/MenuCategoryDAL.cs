﻿using Restaurant.Converters;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.DataAccessLayer
{
    class MenuCategoryDAL
    {
        internal ObservableCollection<MenuCategory> GetAllActiveCategories()
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("spMenuCategory_GetAllActive", con);
                ObservableCollection<MenuCategory> result = new ObservableCollection<MenuCategory>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    MenuCategory menuCategory = new MenuCategory();
                    menuCategory.MenuCategoryId = (int)(reader[0]);//reader.GetInt(0);
                    menuCategory.Name = reader.GetString(1);//reader[1].ToString();
                    menuCategory.Image = reader.IsDBNull(2) ? null : ImageConvertor.ObjectToByteArray( reader[2]) ;
                    menuCategory.Active = (bool)(reader[3]);
                    result.Add(menuCategory);
                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }
    }
}
