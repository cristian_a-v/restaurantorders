﻿using Restaurant.Models.EntityLayer;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.DataAccessLayer
{
    class UserDAL
    {
        internal void AddUser(User user)
        {
            using (SqlConnection con = DALHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("spAddUser", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramFirstName = new SqlParameter("@UserFirstName", user.FirstName);
                SqlParameter paramLastName = new SqlParameter("@UserLastName", user.LastName);
                SqlParameter paramEmail = new SqlParameter("@UserEmail", user.Email);
                SqlParameter paramPhoneNumber = new SqlParameter("@UserPhoneNumber", user.PhoneNumber);
                SqlParameter paramAddress = new SqlParameter("@UserAddress", user.Address);
                SqlParameter paramPassword = new SqlParameter("@UserPassword", user.Password);
                cmd.Parameters.Add(paramFirstName);
                cmd.Parameters.Add(paramLastName);
                cmd.Parameters.Add(paramEmail);
                cmd.Parameters.Add(paramPhoneNumber);
                cmd.Parameters.Add(paramAddress);
                cmd.Parameters.Add(paramPassword);
                con.Open();
                cmd.ExecuteNonQuery();
                //user.PersonID = paramIdPersoana.Value as int?;
            }
        }

        internal bool IsUserWithEmail(string email)
        {
            using (SqlConnection connection = DALHelper.Connection)
            {
                bool result = false;
                SqlCommand cmd = new SqlCommand("spIsUserWithEmail", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter paramEmail = new SqlParameter("@Email", email);
                cmd.Parameters.Add(paramEmail);
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result = Convert.ToBoolean(reader[0]);
                }
                reader.Close();
                return result;
            }
        }

        internal User GetUserEmailPassword(string email,string password)
        {
            using (SqlConnection connection = DALHelper.Connection)
            {
                User user = new User();
                SqlCommand cmd = new SqlCommand("spGetUserEmailPassword", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter paramEmail = new SqlParameter("@Email", email);
                SqlParameter paramPassword = new SqlParameter("@Password", password);
                cmd.Parameters.Add(paramEmail);
                cmd.Parameters.Add(paramPassword);
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    user.UserID = (int)reader[0];
                    user.FirstName = reader[1].ToString();
                    user.LastName = reader[2].ToString();
                    user.Email = reader[3].ToString();
                    user.PhoneNumber = reader[4].ToString();
                    user.Address = reader[5].ToString();
                    user.Password = reader[6].ToString();

                    }
                reader.Close();
                return user;
                }
                
            }

        internal User GetAdminEmailPassword(string email, string password)
        {
            using (SqlConnection connection = DALHelper.Connection)
            {
                User user = new User();
                SqlCommand cmd = new SqlCommand("spGetAdminEmailPassword", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter paramEmail = new SqlParameter("@Email", email);
                SqlParameter paramPassword = new SqlParameter("@Password", password);
                cmd.Parameters.Add(paramEmail);
                cmd.Parameters.Add(paramPassword);
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    user.UserID = (int)reader[0];
                    user.FirstName = reader[1].ToString();
                    user.LastName = reader[2].ToString();
                    user.Email = reader[3].ToString();
                    user.PhoneNumber = reader[4].ToString();

                }
                reader.Close();
                return user;
            }

        }
    }
    }

