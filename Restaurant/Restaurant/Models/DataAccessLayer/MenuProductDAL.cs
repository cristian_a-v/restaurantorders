﻿using Restaurant.Converters;
using Restaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.DataAccessLayer
{
    class MenuProductDAL
    {
        internal ObservableCollection<MenuProduct> GetAllMenuProducts()
        {
            SqlConnection con = DALHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("spMenuCategory_GetAllActive", con);
                ObservableCollection<MenuProduct> result = new ObservableCollection<MenuProduct>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    MenuProduct menuProduct = new MenuProduct();
                    menuProduct.MenuProductId = (int)(reader[0]);//reader.GetInt(0);
                    menuProduct.Name = reader.GetString(2);//reader[1].ToString();
                    menuProduct.Description = reader.GetString(3);
                    menuProduct.Image = reader.IsDBNull(4) ? null : ImageConvertor.ObjectToByteArray(reader[2]);
                    menuProduct.Active = (bool)(reader[5]);
                    menuProduct.Price = (decimal)reader[6];
                    result.Add(menuProduct);
                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }


        internal ObservableCollection<MenuProduct> GetAllMenuProductsForCategory(MenuCategory category)
        {
            using (SqlConnection connection = DALHelper.Connection)
            {
                ObservableCollection<MenuProduct> result = new ObservableCollection<MenuProduct>();
                SqlCommand cmd = new SqlCommand("spGetMenuProductsForCategory", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter categoryId = new SqlParameter("@CategoryId", category.MenuCategoryId);
                cmd.Parameters.Add(categoryId);
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(new MenuProduct()
                    {
                        MenuProductId = reader.GetInt32(0),
                        MenuCategoryId = reader.GetInt32(1),
                        Name = reader.GetString(2),
                        Description = reader.GetString(3),
                        Image = ImageConvertor.ObjectToByteArray(reader[4]),
                        Active = (bool)(reader[5]),
                        Price = (decimal)(reader[6])

                });
                }
                return result;
            }
        }

        internal ObservableCollection<Product> GetProductsForMenuProduct(MenuProduct product)
        {
            using (SqlConnection connection = DALHelper.Connection)
            {
                ObservableCollection<Product> result = new ObservableCollection<Product>();
                SqlCommand cmd = new SqlCommand("spGetProductsForMenuProduct", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter MenuProductId = new SqlParameter("@MenuProductId", product.MenuProductId);
                cmd.Parameters.Add(MenuProductId);
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(new Product()
                    {
                        ProductId = (int)reader[0],
                        Name = (string)reader[1],
                        Quantity = (int)reader[2]

                    });
                }
                return result;
            }
        }

        internal ObservableCollection<MenuProduct> GetProductsByName(string name)
        {
            using (SqlConnection connection = DALHelper.Connection)
            {
                ObservableCollection<MenuProduct> result = new ObservableCollection<MenuProduct>();
                SqlCommand cmd = new SqlCommand("spSearchMenuProductByName", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter categoryId = new SqlParameter("@NameLike", name);
                cmd.Parameters.Add(categoryId);
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(new MenuProduct()
                    {
                        MenuProductId = reader.GetInt32(0),
                        MenuCategoryId = reader.GetInt32(1),
                        Name = reader.GetString(2),
                        Description = reader.GetString(3),
                        Image = ImageConvertor.ObjectToByteArray(reader[4]),
                        Active = (bool)(reader[5]),
                        Price = (decimal)(reader[6])

                    });
                }
                return result;
            }
        }
    }
}
