﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.EntityLayer
{
    class MenuCategory : BasePropertyChanged
    {
        private int? menuCategoryId;

        public int? MenuCategoryId
        {
            get
            {
                return menuCategoryId;
            }
            set
            {
                menuCategoryId = value;
                NotifyPropertyChanged("MenuCategoryId");
            }
        }

        private string name;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                NotifyPropertyChanged("Name");
            }
        }

        private bool active;

        public bool Active
        {
            get
            {
                return active;
            }
            set
            {
                active = value;
                NotifyPropertyChanged("Active");
            }
        }

        private byte[] image;

        public byte[] Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
                NotifyPropertyChanged("Image");
            }
        }
    }
}
