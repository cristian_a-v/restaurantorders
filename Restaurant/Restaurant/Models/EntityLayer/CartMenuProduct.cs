﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.EntityLayer
{
    class CartMenuProduct
    {
        public MenuProduct MenuProduct { get; set; }
        
        public int Quantity { get; set; }
    }
}
