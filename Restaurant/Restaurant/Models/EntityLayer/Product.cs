﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.EntityLayer
{
    class Product : BasePropertyChanged
    {
        private int? productId;

        public int? ProductId
        {
            get
            {
                return productId;
            }
            set
            {
                productId = value;
                NotifyPropertyChanged("ProductId");
            }
        }

        private string name;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                NotifyPropertyChanged("Name");
            }
        }

        private int quantity;

        public int Quantity
        {
            get
            {
                return quantity;
            }
            set
            {
                quantity = value;
                NotifyPropertyChanged("Quantity");
            }
        }


    }
}
