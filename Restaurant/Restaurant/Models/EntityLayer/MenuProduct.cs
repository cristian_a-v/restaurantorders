﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Models.EntityLayer
{
    class MenuProduct : BasePropertyChanged
    {
        private int? menuProductId;

        public int? MenuProductId
        {
            get
            {
                return menuProductId;
            }
            set
            {
                menuProductId = value;
                NotifyPropertyChanged("MenuProductId");
            }
        }

        private int? menuCategoryId;

        public int? MenuCategoryId
        {
            get
            {
                return menuCategoryId;
            }
            set
            {
                menuCategoryId = value;
                NotifyPropertyChanged("MenuCategoryId");
            }
        }

        private string name;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                NotifyPropertyChanged("Name");
            }
        }

        private bool active;

        public bool Active
        {
            get
            {
                return active;
            }
            set
            {
                active = value;
                NotifyPropertyChanged("Active");
            }
        }

        private byte[] image;

        public byte[] Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
                NotifyPropertyChanged("Image");
            }
        }

        private string description;

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
                NotifyPropertyChanged("Description");
            }
        }

        private decimal price;
        public decimal Price
        {
            get
            {
                return price / 1.000000000000000000000000000000000m; ;
            }
            set
            {
                price = value;
                NotifyPropertyChanged("Price");
            }
        }

    }
}


